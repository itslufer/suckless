
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] =
  {
      
     /* 8 normal colors */
      [0] = "#282828", /* hard contrast: #1d2021 / soft contrast: #32302f */
      [1] = "#cc241d", /* red     */
      [2] = "#98971a", /* green   */
      [3] = "#d79921", /* yellow  */
      [4] = "#458588", /* blue    */
      [5] = "#b16286", /* magenta */
      [6] = "#689d6a", /* cyan    */
      [7] = "#a89984", /* white   */

      /* 8 bright colors */
      [8]  = "#928374", /* black   */
      [9]  = "#fb4934", /* red     */
      [10] = "#b8bb26", /* green   */
      [11] = "#fabd2f", /* yellow  */
      [12] = "#83a598", /* blue    */
      [13] = "#d3869b", /* magenta */
      [14] = "#8ec07c", /* cyan    */
      [15] = "#ebdbb2", /* white   */

      [256] = "#282C34",  // Background
      [257] = "#bbc2cf", //Foreground

     /* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", /* default foreground colour */
	"black", /* default background colour */
  };

/*
 * Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
unsigned int defaultcs = 257;
static unsigned int defaultrcs = 257;
