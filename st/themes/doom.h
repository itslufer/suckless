
/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] =
  {
   [0] = "#282C34", // black
   [1] = "#ff6c6b", // dark red
   [2] = "#98be65", // dark green
   [3] = "#DA8548", // dark orange
   [4] = "#51afef", // dark blue
   [5] = "#C678DD", // dark magenta
   [6] = "#46D9FF", // dark cyan 
   [7] = "#202328", // grey   

   [8] = "#5B6268", // dark grey
   [9] = "#ff6c6b", // red
   [10] = "#98be65", // green
   [11] = "#DA8548", // orange
   [12] = "#51afef", // blue
   [13] = "#C678DD", // magenta
   [14] = "#46D9FF", // cyan
   [15] = "#bbc2cf", // white

   [256] = "#282C34",  // Background
   [257] = "#bbc2cf", //Foreground

     /* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", /* default foreground colour */
	"black", /* default background colour */
  };

/*
 * Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
unsigned int defaultcs = 257;
static unsigned int defaultrcs = 257;
