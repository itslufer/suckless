/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
    /* 8 normal colors */
    [0] = "#2F3239", /* black   */
    [1] = "#e85c51", /* red     */
    [2] = "#7aa4a1", /* green   */
    [3] = "#fda47f", /* yellow  */
    [4] = "#5a93aa", /* blue    */
    [5] = "#ad5c7c", /* magenta */
    [6] = "#a1cdd8", /* cyan    */
    [7] = "#ebebeb", /* white   */
                                  
    /* 8 bright colors */
    [8] = "#4c566a", /* black   */
    [9] = "#e85c51", /* red     */
    [10] = "#7aa4a1", /* green   */
    [11] = "#fda47f", /* yellow  */
    [12] = "#5a93aa", /* blue    */
    [13] = "#ad5c7c", /* magenta */
    [14] = "#8fbcbb", /* cyan    */
    [15] = "#eceff4", /* white   */
                                      
    /* special colors */
    [256] = "#282d35", /* background */
    [257] = "#e6eaea", /* foreground */

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", /* default foreground colour */
	"black", /* default background colour */
};


/*
 * Default colors (colorname index)
 * foreground, background, cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
unsigned int defaultcs = 257;
static unsigned int defaultrcs = 257;

