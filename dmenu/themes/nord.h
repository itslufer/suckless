static const char *colors[SchemeLast][2] = {
	//     fg         bg
	[SchemeNorm] = { "#bbbbbb", "#282C34" },
	[SchemeSel] = { "#CBD4DA", "#384a5b" },
	[SchemeOut] = { "#000000", "#384a5b" },
};

