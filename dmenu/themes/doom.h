static const char *colors[SchemeLast][2] = {
	//     fg         bg
	[SchemeNorm] = { "#bbbbbb", "#282C34" },
	[SchemeSel] = { "#000000", "#51afef" },
	[SchemeOut] = { "#000000", "#51afef" },
};

