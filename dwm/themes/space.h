// Cor JANELA inativa
static const char col_gray1[]       = "#1C1C1C";

// Cor da JANELA Ativa
static const char col_cyan[]        = "#5b3728";

// BORDA Janela Ativa
static const char col_gray5[]       = "#ac795e";

// BORDA Janela Inativa
static const char col_gray2[]       = "#342828";

// Cor TEXTO Números 
static const char col_gray3[]       = "#ffffff";

// Cor TEXTO Janela Ativa
static const char col_gray4[]       = "#ffffff";


