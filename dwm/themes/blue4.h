/* fundo de toda a barra */
/* tem que ser escura */
static const char col_gray1[]       = "#3a3b44";
/* Cor da borda de uma janela inativa */
/* tem que ser escura */
static const char col_gray2[]       = "#191919";
/* texto */
/* tem que ser claro */
// static const char col_gray3[]       = "#f8f8f2";
// static const char col_gray4[]       = "#f8f8f2"; /* segue a mesma lógica: tem que ser claro */
static const char col_gray3[]       = "#c8b9e8";
static const char col_gray4[]       = "#c8b9e8"; /* segue a mesma lógica: tem que ser claro */

/* Bordas de uma janela ativa */
static const char col_gray5[]       = "#AEB0D6";
/* camada acima do fundo escuro */
/* tem que ser claro */
static const char col_cyan[]        = "#535266";
/* static const char col_cyan[]        = "#2a2a7d"; */
/* static const char col_cyan[]        = "#5d65aa"; */

