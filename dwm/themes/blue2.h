/* BARRA */
/* fundo de toda a barra */
static const char col_gray1[]       = "#000000";
/* camada acima do fundo escuro */
static const char col_cyan[]        = "#6F6F6F";

/* TEXTO */
/* Espaços de trabalho INATIVOS */
static const char col_gray3[]       = "#D3CCBB";
/* Espaço de trabalho ATIVOS */
static const char col_gray4[]       = "#000000"; 

/* JANELA */
/* Cor da borda de uma janela INATIVA*/
static const char col_gray2[]       = "#000000";
/* Bordas de uma janela ATIVA */
static const char col_gray5[]       = "#D3CCBB";




