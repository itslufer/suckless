/* BARRA */
/* fundo de toda a barra */
static const char col_gray1[]       = "#1F1F1F";
/* camada acima do fundo escuro */
static const char col_cyan[]        = "#000000";

/* TEXTO */
/* Espaços de trabalho INATIVOS */
static const char col_gray3[]       = "#000000";
/* Espaço de trabalho ATIVOS */
static const char col_gray4[]       = "#D3CCBB"; 

/* JANELA */
/* Cor da borda de uma janela INATIVA*/
static const char col_gray2[]       = "#000000";
/* Bordas de uma janela ATIVA */
static const char col_gray5[]       = "#D3CCBB";




