// Cor JANELA inativa
static const char col_gray1[]       = "#2f3537";

// Cor da JANELA Ativa
static const char col_cyan[]        = "#040d1c";

// BORDA Janela Ativa
static const char col_gray5[]       = "#7d909d";

// BORDA Janela Inativa
static const char col_gray2[]       = "#2f3537";

// Cor TEXTO Números 
static const char col_gray3[]       = "#b5d5f5";

// Cor TEXTO Janela Ativa
static const char col_gray4[]       = "#ffffff";


