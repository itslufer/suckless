// Cor JANELA inativa
static const char col_gray1[]       = "#1C1C1C";

// Cor da JANELA Ativa
static const char col_cyan[]        = "#384a5b";

// BORDA Janela Ativa
static const char col_gray5[]       = "#5E81AC";

// BORDA Janela Inativa
static const char col_gray2[]       = "#282c34";

// Cor TEXTO Números 
static const char col_gray3[]       = "#CBD4DA";

// Cor TEXTO Janela Ativa
static const char col_gray4[]       = "#CBD4DA";


