// Cor JANELA inativa
static const char col_gray1[]       = "#37302f";

// Cor da JANELA Ativa
static const char col_cyan[]        = "#1C1C1C";

// BORDA Janela Ativa
static const char col_gray5[]       = "#7d9d91";

// BORDA Janela Inativa
static const char col_gray2[]       = "#37302f";

// Cor TEXTO Números 
static const char col_gray3[]       = "#ffffff";

// Cor TEXTO Janela Ativa
static const char col_gray4[]       = "#ffffff";


