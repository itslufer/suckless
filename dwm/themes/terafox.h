/* fundo de toda a barra */
/* tem que ser escura */
static const char col_gray1[]       = "#22222E";
/* Cor da borda de uma janela inativa */
/* tem que ser escura */
static const char col_gray2[]       = "#2E3440";
/* texto */
/* tem que ser claro */
static const char col_gray3[]       = "#B2B9E3";
static const char col_gray4[]       = "#B2B9E3"; /* segue a mesma lógica: tem que ser claro */

/* Bordas de uma janela ativa */
static const char col_gray5[]       = "#5E81AC";
/* camada acima do fundo escuro */
/* tem que ser claro */
static const char col_cyan[]        = "#38415b";

